Name:          tpm2-tools
Version:       5.7
Release:       2
Summary:       A TPM2.0 testing tool based on TPM2.0-TSS
License:       BSD-3-Clause
URL:           https://github.com/tpm2-software/tpm2-tools
Source0:       https://github.com/tpm2-software/tpm2-tools/releases/download/%{version}/%{name}-%{version}.tar.gz
Patch0:        tpm2-tools-5.7-do-not-exit-when-missing-pandoc.patch
Patch1:        Hygon-add-ecc-encrypt-decrypt-commands-supp.patch

BuildRequires: gcc-c++ libtool
BuildRequires: pkgconfig(bash-completion)
BuildRequires: pkgconfig(cmocka)
BuildRequires: pkgconfig(efivar)
BuildRequires: pkgconfig(libcrypto) >= 1.1.0
BuildRequires: pkgconfig(libcurl)
BuildRequires: pkgconfig(tss2-esys) >= 2.4.0
BuildRequires: pkgconfig(tss2-fapi)
BuildRequires: pkgconfig(tss2-mu)
BuildRequires: pkgconfig(tss2-rc)
BuildRequires: pkgconfig(tss2-sys)
BuildRequires: pkgconfig(tss2-tctildr)
Requires:  tpm2-tss >= 2.4.0
Obsoletes: tpm2-tools-help < %{version}-%{release}

%description
The package contains the code for the TPM (Trusted Platform Module) 2.0
tools based on tpm2-tss.

The tpm2-tools projects aims to deliver both low-level and aggregate
command line tools that provide access to a tpm2.0 compatible device.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -vif
%configure --disable-static --disable-silent-rules
%make_build

%install
%make_install

%check
%make_build check

%files
%license docs/LICENSE
%doc docs/README.md docs/CHANGELOG.md
%{_bindir}/*
%{_datadir}/bash-completion/completions/tpm2*
%{_datadir}/bash-completion/completions/tss2*
%{_mandir}/*/*

%changelog
* Fri Aug 2 2024 chench <chench@hygon.cn> - 5.7-2
- add ecc encrypt/decrypt commands support

* Wed Oct 02 2024 Funda Wang <fundawang@yeah.net> - 5.7-1
- update to 5.7

* Fri Jun 21 2024 bianxiuning <bianxiuning@kylinos.cn> - 5.5-3
- revert sm2 sign and verifysignature

* Thu May 02 2024 cenhuilin <cenhuilin@kylinos.cn> - 5.5-2
- fix CVE-2024-29038 CVE-2024-29039

* Tue Jul 18 2023 jinlun <jinlun@huawei.com> - 5.5-1
- update to 5.5

* Tue Dec 20 2022 wangyunjia <yunjia.wang@huawei.com> - 5.4-1
- update to 5.4

* Mon Sep 27 2021 fuanan <fuanan3@huawei.com> - 5.0-5
- fix CVE-2021-3565

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 5.0-4
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Mon Jul 26 2021 fuanan <fuanan3@huawei.com> - 5.0-3
- Remove redundant gdb from BuildRequires

* Fri Apr 30 2021 Hugel <gengqihu1@huawei.com> - 5.0-2
- fix segmentation fault on tpm2

* Mon Jan 25 2021 panxiaohe <panxiaohe@huawei.com> - 5.0-1
- update to 5.0

* Thu Aug 6 2020 zhangxingliang <zhangxingliang3@huawei.com> - 4.1.3-1
- update to 4.1.3

* Thu Mar 19 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.1.1-7
- add BuildRequires: libgcrypt-devel gdb

* Wed Jan 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.1.1-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: add BuildRequires

* Tue Sep 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.1.1-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: revise spec file with new rules

* Wed Aug 14 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.1.1-4
- Package init
